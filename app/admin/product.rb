ActiveAdmin.register Product do

  permit_params :title, :description, :price, :category_id

  index do
    selectable_column

    column :id

    column 'Title' do |product|
      link_to product.title, admin_product_path(product)
    end

    column :price
  end

end
